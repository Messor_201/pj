unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids, Main, Unit2;

type
  THum = record
    Ext:boolean; //
    ModT: array of TAsp;//
  end;

  { TForm2 }

  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ChengeAspBt: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    IntuitionEd: TEdit;
    IntuitionEd1: TEdit;
    IntuitionEd2: TEdit;
    LogicEd: TEdit;
    EthicsEd: TEdit;
    SensEd: TEdit;
    isExtrRb: TRadioButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    StringGrid1: TStringGrid;
    Label5: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
  function forCase(i:integer): integer;
   procedure EthicsEdChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Headers();
    procedure FromFormToDon();
    procedure ChengeAspBtClick(Sender: TObject);
    procedure IntuitionEd1Change(Sender: TObject);
    procedure isExtrRbChange(Sender: TObject);

    procedure Ostove(var a:THum) ;
    procedure DoAspect(var a:Tasp);
    procedure HF();
    procedure HF1();
    procedure InAspects();
    procedure IntoFiltr();
    procedure PrintForActive();
    procedure PrintForInActive();
    function BoolToStrHF (const value : boolean) : string;
    procedure DoNames();

    procedure Replacement1();
    procedure Replacement2();
    procedure Replacement3();
    procedure Replacement4();

    procedure Button11();
    procedure Button22();
    procedure Button33();
    procedure Button44();
  private

  public

  end;
  var
  Form2: TForm2;
  Intuit:TAsp;
  don:THum;


implementation

{$R *.lfm}
function TForm2.BoolToStrHF (const value : boolean) : string;
begin
  if value
  then
    Result := '- т.е. высокочастотная'
  else
    Result := '- т.е. низкочастотная';
end;

procedure TForm2.PrintForInActive();
var i:integer;
begin
for I := 0 to 3 do
  If (Don.ModT[i].ExIsHF)then StringGrid1.Cells[2*i +2,3]:='высокочастотн. выход'
   else  StringGrid1.Cells[2*i +2,3]:='низкочастотный выход';
end;


procedure TForm2.PrintForActive();
var i:integer;
begin
for I := 1 to 4 do
  If (Don.ModT[i-1].IntIsHF)then
  StringGrid1.Cells[2*i ,2]:='высокочастотный вход'
  else  StringGrid1.Cells[2*i,2]:='низкочастотный вход';
end;

procedure  TForm2.InAspects();
var i:integer;
begin
  for i := 1 to 4 do
begin
  DoAspect(Don.ModT[i-1]);
  if Don.ModT[i-1].active then
  begin
  StringGrid1.Cells[2*i,4]:='аспект включился' ;
  Don.ModT[i].Inf.Info:= Don.ModT[i-1].Inf.Info + Don.ModT[i].Inf.Info;
  end
   else  StringGrid1.Cells[2*i ,4]:='аспект не активен';
  end;
end;

procedure Tform2.HF();
var i:integer;
a:Tasp;
begin
   with StringGrid1 do
begin
   // Cells[2,1]:='proba';
   for i:= 1 to 4 do
   begin
   IsAspHF(Don.ModT[i-1]);
  Cells[i*2,1]:= (inttostr(Don.ModT[i-1].Inf.Info)
  +' ' + BoolToStrHF(Don.ModT[i-1].Inf.InfIsHF));
   end;
    end;

end;

procedure Tform2.HF1();
var i:integer;
begin
   with StringGrid1 do
begin
   for i:= 1 to 4 do
  Cells[i*2,5]:=
  (inttostr(Don.ModT[i-1].Inf.Info)
  +' ' + BoolToStrHF(Don.ModT[i-1].Inf.InfIsHF));
    end;

end;


procedure Tform2.IntoFiltr();
begin

 PrintForActive();
 PrintForInActive();

end;

procedure Tform2.Headers();
begin
with StringGrid1 do
begin
  Cells[0,1]:= 'инт-ь входящ инфы' ;
  Cells[0,2]:= 'вкл высокоинтесивно?' ;
  Cells[0,3]:= 'выкл высокоинт-но ?' ;
  Cells[0,4]:= 'активир-ся на входе?' ;
  Cells[0,5]:=  'инт-ть после взаим-я' ;
end;
end;

procedure TForm2.Button1Click(Sender: TObject);

begin


FromFormToDon();
  Headers();
  HF ();
  InAspects();
  IntoFiltr();
  HF1 ();
  Form3.Enabled := True;
Form3.Show;




end;

procedure TForm2.Button2Click(Sender: TObject);
begin

end;

procedure TForm2.Button11();
var  iOld, iNew:integer;
begin
  iOld:=StrtoInt(Edit1.Text);
  Inew:=iOld+1;
end;
procedure TForm2.Button22();
var gOld, gNew:integer;
begin
 gOld:=StrtoInt(Edit2.Text);
  gnew:=gOld+2;

end;

procedure TForm2.Button33();
var hOld, hNew:integer;
begin
hOld:=StrtoInt(Edit3.Text);
hNew:=hOld+3;

end;

procedure TForm2.Button44();
var aOld, aNew:integer;
begin
aOld:=StrtoInt(Edit4.Text);
aNew:=aOld+4;


end;

procedure TForm2.Button3Click(Sender: TObject);

begin

end;

procedure TForm2.Edit1Change(Sender: TObject);
begin

end;

procedure TForm2.Edit2Change(Sender: TObject);
begin

end;

procedure TForm2.Edit3Change(Sender: TObject);
begin

end;

procedure TForm2.Edit4Change(Sender: TObject);
begin

end;






procedure TForm2.FromFormToDon();
begin
Don.ModT[0].Inf.Info := strtoint(IntuitionEd.Text);
Don.ModT[1].Inf.Info := strtoint(IntuitionEd1.Text);
Don.ModT[2].Inf.Info := strtoint(EthicsEd.Text);
Don.ModT[3].Inf.Info := strtoint(IntuitionEd2.Text);
end;

procedure TForm2.DoAspect(var a:tasp);
begin


end;

 // детальки собираем в остов

procedure Tform2.Ostove(var a:THum) ;
begin
 with a do
begin  //
  if isExtrRb.Checked then
  begin      //
     Don.Ext:=true;
     ModT[0].IntIsHF:=true;
     ModT[0].ExIsHF:=False;
     ModT[1].IntIsHF:=true;
     ModT[1].ExIsHF:=true;
     ModT[2].IntIsHF:=False;
     ModT[2].ExIsHF:=False;
     ModT[3].IntIsHF:=False;
     ModT[3].ExIsHF:=true;
  end
  else     //
  begin
     Don.Ext:=False;
     ModT[0].IntIsHF:=false;
     ModT[0].ExIsHF:=true;
     ModT[1].IntIsHF:=false;
     ModT[1].ExIsHF:=false;
     ModT[2].IntIsHF:=true;
     ModT[2].ExIsHF:=true;
     ModT[3].IntIsHF:=true;
     ModT[3].ExIsHF:=false;
  end;
  ModT[0].numb:=1;
  ModT[1].numb:=2;
  ModT[2].numb:=3;
  ModT[3].numb:=4;
 end;

end;

{ TForm2 }

procedure TForm2.FormCreate(Sender: TObject);
begin

end;







procedure TForm2.ChengeAspBtClick(Sender: TObject);
begin
  Setlength(don.ModT,4);
  Ostove(don);

  Button11();
  Button22();
  Button33();
  Button44();
  Replacement1();
  Replacement2();
  Replacement3();
  Replacement4();

end;

procedure TForm2.IntuitionEd1Change(Sender: TObject);
begin

end;



procedure TForm2.isExtrRbChange(Sender: TObject);
begin

end;





procedure TForm2.DoNames();
begin
 with StringGrid1 do begin

  Cells[2,0] :='интуиция' ;
  Cells[4,0] :='лoгика' ;
  Cells[6,0] :='этика' ;
  Cells[8,0] :='сенсорика' ;
end;
end;
             procedure TForm2.EthicsEdChange(Sender: TObject);
begin

end;
             procedure TForm2.Replacement1();
var iOld, iNew:integer;
    begin
    iOld:=StrtoInt(Edit1.Text);
    iNew:=forCase(iOld);
    with Form2.StringGrid1 do begin
    Cells[iNew,0] :='интуиция' ;
    end;
    iOld:=0;
    iNew:=0;
    Don.modT[inew].name:='интуиция' ;
end;
  procedure Tform2.Replacement2();
  var iOld, iNew:integer;
    begin
    iOld:=StrtoInt(Edit2.Text);
    iNew:=forCase(iOld);
    with Form2.StringGrid1 do begin
     Cells[iNew,0] :='лoгика';

    end;
    iOld := 0;
    iNew := 0;
    Don.modT[inew].name:='логика' ;
  end;

  procedure Tform2.Replacement3();
  var  iOld, iNew:integer;
    begin
    iOld:=StrtoInt(Edit3.Text);
    iNew:=forCase(iOld);
    with Form2.StringGrid1 do begin
    Cells[iNew,0] :='этика' ;
   end;
    iOld := 0;
    iNew := 0;
    Don.modT[inew].name:='этика' ;
  end;

  procedure Tform2.Replacement4();
 var  iOld, iNew:integer;
    begin
    iOld:=StrtoInt(Edit4.Text);
    iNew:=forCase(iOld);
    with Form2.StringGrid1 do begin
    Cells[iNew,0] :='сенсорика' ;
    end;
    iOld := 0;
    iNew := 0;
    Don.modT[inew].name:='сенсорика' ;
end;
   function Tform2.forCase(i:integer) :integer;
   begin
   case i of
   1 : Result :=2;
   2 : Result :=4;
   3 : Result :=6;
   4 : Result :=8;
   end;
   end;


end.

