object TableModT: TTableModT
  Left = 0
  Top = 0
  Caption = 'TableModT'
  ClientHeight = 387
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PrepareAspects: TButton
    Left = 470
    Top = 304
    Width = 163
    Height = 25
    Caption = #1089#1086#1079#1076#1072#1090#1100' '#1072#1089#1087#1077#1082#1090#1099
    TabOrder = 0
    OnClick = PrepareAspectsClick
  end
  object GoAspects: TButton
    Left = 470
    Top = 243
    Width = 163
    Height = 25
    Caption = #1054#1073#1098#1077#1076#1077#1085#1080#1090#1100' '#1072#1089#1087#1077#1082#1090#1099
    TabOrder = 1
    OnClick = GoAspectsClick
  end
  object ExtrRbt: TRadioButton
    Left = 392
    Top = 281
    Width = 113
    Height = 17
    Caption = #1101#1082#1089#1090#1088#1072#1074#1077#1088#1090'?'
    TabOrder = 2
  end
  object StringGrid1: TStringGrid
    Left = 8
    Top = 8
    Width = 737
    Height = 201
    ColCount = 12
    TabOrder = 3
    ColWidths = (
      64
      51
      69
      44
      72
      47
      74
      46
      81
      50
      50
      80)
    RowHeights = (
      24
      24
      46
      24
      24)
  end
  object IntuitionEd: TEdit
    Left = 48
    Top = 232
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '1'
    TextHint = #1048#1085#1090#1091#1080#1094#1080#1103
  end
  object LogicEd: TEdit
    Left = 48
    Top = 272
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '1'
    TextHint = #1051#1086#1075#1080#1082#1072
  end
  object EthicsEd: TEdit
    Left = 48
    Top = 312
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '1'
    TextHint = #1069#1090#1080#1082#1072
  end
  object SensEd: TEdit
    Left = 48
    Top = 349
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '1'
    TextHint = #1057#1077#1085#1089#1086#1088#1080#1082#1072
  end
end
